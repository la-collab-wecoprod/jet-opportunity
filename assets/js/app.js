$(document).ready(function () {

    // Datepicker
    $('.datepicker').datepicker({
        autoSize: true,
        prevText: '',
        nextText: '',
    });

    // Drawer
    $('.drawer > div').hide();

    $('*[data-trigger]').on('click', function (e) {
        e.preventDefault();

        // Background du drawer
        let fullBackground = $('<div>')
            .addClass('full-background')
            .on('click', function () {
                $('.drawer').removeClass('drawer--active');
                $('.drawer > div').hide();
                $('.full-background').remove();
            });
        $('body').append(fullBackground);

        $('.drawer').addClass('drawer--active');
        $('.drawer > div[data-target="' + $(this).data('trigger') + '"]').show();
        $('.full-background').addClass('full-background--active');
    });

    // Affichage des menus
    $('*[data-menu-trigger]').on('click', function (e) {
        e.preventDefault();

        let menuClassName = $(this).data('menu-trigger');

        // Background du drawer
        let fullBackground = $('<div>')
            .addClass('full-background')
            .on('click', function () {
                $('.' + menuClassName).removeClass(menuClassName + '--active');
                $('.full-background').remove();
            });
        $('body').append(fullBackground);

        $('.' + menuClassName).addClass(menuClassName + '--active');
        $('.full-background').addClass('full-background--active');
    });
});