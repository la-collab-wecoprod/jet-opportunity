# Jet Opportunity

Lien vers la
maquette: [Figma](https://www.figma.com/file/RKHqd5BrEIXWgzS6dObHG2/V3?node-id=413%3A9051&t=NqlvR7pN4Is9qwZS-0)

Vous pouvez retrouver les composants de l'application sur cette page : [Design system](design-system.html)

Toutes les pages de l'application :

* [Page de recherche](homepage.html)
* [Résultats de recherche](search.html)
* [Page d'un résultat](result.html)
* [Panier](cart.html)
* [Demande de devis](cart-quote.html)
* [Réservations](bookings.html)
* [Compte](account.html)
* [Connexion](sign.html)
* [Page de contenu](content.html)

## Notes

* Les pages de l'application sont des exemples de pages.
* La police "Poppins" n'est pas alignée correctement sur certains éléments (les badges par exemple).
* Les icônes ont été "flatisées" pour faciliter leur intégration. Elles sont noires par défaut et je leur ai appliqué
  des filtres CSS pour les colorer.
* Le fichier `assets/js/app.js` permet d'ajouter les interactions de l'application : génération du calendrier via un composant jQuery
  UI, affichage des drawers, affichage des menus latéraux, etc.